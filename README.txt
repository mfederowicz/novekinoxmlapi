Novekino XmlApi
===============
Ficzery
1. Parser odpalamy przy pomocy skryptu: run-parser.php, domyślnie metoda: $x->doStuff();pobiera filmy z danego dnia,
jeżeli wstawimy jako parametr dla metody doStuff wartość true, to wtedy pobierane są filmy z kolejnych 4 dni licząc
od dnia dzisiejszego.
2. Interfejs Api:
 a. Api odpalane jest z poziomu pliku index.php, nie należy w nim nic modyfikować
 b. Domyślne odpalenie api powoduje pobranie z bazy wszystkich repertuarów z kolejnych 4 dni licząc od dnia
 dzisiejszego.
 c. Pobieranie repertuaru z wybranego dnia: day/n , gdzie n to liczba oznaczająca dzień, możliwe są wartości 0-4
 d. Pobieranie repertuaru z wybranych dni: fromdays/a,b,c,d  gdzie a,b,c,d to liczby oznaczająca dzień,
 możliwe są wartości 0-4. W przypadku podania wartości w stylu 1,1,2,2,3 zostaną wybrane tylko repoertuary z
 dni 1,2,3 po prostu usuwane są duplikaty przy generowaniu zapytania
3. Obsługiwane formaty xml i json. Domyślnie ustawiony jest tryb xml. Pomiędzy formatami przełączamy się przy pomocy
nagłówka 'Content-Type'. Dozwolone wartości to: application/xml oraz application/json inne content-types są traktowane
jako błąd i wyświetlany jest odpowiedni komunikat

