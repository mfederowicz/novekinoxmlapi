<?php
//ustawienie redirect_path_info
if(array_key_exists('PATH_REDIRECTED',$_SERVER)){
    $_SERVER['REDIRECT_PATH_INFO'] = $_SERVER['PATH_REDIRECTED'];
}
if(array_key_exists('PATH_INFO',$_SERVER)){
    $_SERVER['REDIRECT_PATH_INFO'] = $_SERVER['PATH_INFO'];
}elseif(!array_key_exists('PATH_INFO',$_SERVER)){
    $_SERVER['REDIRECT_PATH_INFO'] = $_SERVER['REQUEST_URI'];
}


defined('APPLICATION_PATH')
    || define('APPLICATION_PATH', realpath(dirname(__FILE__)));

require_once 'vendor/autoload.php';
require_once 'vendor/novekinoxmlapi/Api.php';
$rest = new Novekinoxmlapi_Api();
$rest->handlerequest();
