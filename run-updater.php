<?php
defined('APPLICATION_PATH')
    || define('APPLICATION_PATH', realpath(dirname(__FILE__)));
require_once 'vendor/autoload.php';
require_once 'vendor/novekinoxmlapi/Parser.php';
$x = new Novekinoxmlapi_Parser();
$x->updateMoviesDescriptions();