<?php
require_once 'libs/phpQuery-onefile.php';
require_once 'libs/objectslib.php';
require_once 'libs/jaxbxmlparser.php';
require_once 'libs/KLogger.php';
require_once 'config.php';
require_once 'Exceptions.php';
require_once 'SafePDO.php';

use Guzzle\Http\Client;

function error_handler($code, $message, $file, $line)
{
    if (0 == error_reporting()) {
        return;
    }
    throw new ErrorException($message, 0, $code, $file, $line);
}

function exception_handler($e)
{
    echo "Exception: ", $e->getMessage(), "\n";
}

set_error_handler("error_handler");
set_exception_handler("exception_handler");

abstract class Novekinoxmlapi_Abstract
{
    /**
     * Returns version
     *
     * @return string
     */
    public function getVersion()
    {
        return 0.1;
    }

    public $_logger;
    public $_config;
    public $_translate;
    public $_client;
    public $_db;

    public $_repertuar_url = "http://www.novekino.pl/kina/siedlce/repertuar.php?data=";
    public $_movie_url = "http://www.novekino.pl/kina/siedlce/film.php?id=";
    public $_maxdays = 4;

    protected $_username;
    protected $_token;

    public function construct()
    {
        $this->_db = $this->getDb();


    }


    public function getClient()
    {
        $tmp = new Client();
        $tmp->setUserAgent("Mozilla/5.0 (Windows NT 6.1; WOW64; rv:20.0) Gecko/20100101 Firefox/20.0");

        return $tmp;
    }

    public function getDb()
    {
        global $apiConfig;
        $config = $apiConfig;
        $host = $config['db']['host'];
        $dbname = $config['db']['dbname'];
        $dbusername = $config['db']['username'];
        $dbpassword = $config['db']['password'];
        try {
            $this->_db = new SafePDO("mysql:host=$host; dbname=$dbname", $dbusername, $dbpassword);
            $this->_db->exec("SET CHARACTER SET utf8"); // Sets encoding UTF-8

        } catch (Exception $e) {
            throw new Novekinoxmlapi_Exception("Problem with db connection");
        }

        return $this->_db;
    }


    public function getToken()
    {
        return $this->_token;
    }

    public function getUser()
    {
        return $this->_username;
    }

    public function getParser()
    {

        $restoptions = array('objtagprefix' => null, 'collectiontag' => 'list');
        $parser = new Jaxbxmlparser($restoptions);
        return $parser;
    }


    public function getLogger()
    {
        global $apiConfig;
        $config = $apiConfig;
        $log = new KLogger($config['log'], KLogger::INFO);
        return $log;

    }



    public function enc($string)
    {
        return mb_convert_encoding($string, "UTF-8", "ISO-8859-2");
    }

    public function denc($string)
    {
        return mb_convert_encoding($string, "ISO-8859-2", "UTF-8");
    }

}
