<?php
/**
 * @see Novekinoxmlapi_Abstract
 */

require_once 'Abstract.php';
use Guzzle\Http\Client;

class Novekinoxmlapi_Api extends Novekinoxmlapi_Abstract
{

    /**
     * Whether or not {@link handle()} should send output or return the response.
     * @var boolean Defaults to false
     */
    protected $_returnResponse = false;

    public $_headers = array();

    /**
     * @var string Current Method
     */
    protected $_method;

    /**
     * @var string Encoding
     */
    protected $_encoding = 'UTF-8';

    /**
     * Class constructor
     */
    public function __construct()
    {
        $request_uri = array_key_exists('REDIRECT_PATH_INFO',$_SERVER)?$_SERVER['REDIRECT_PATH_INFO']:"/";
        $query_string = $_SERVER['QUERY_STRING'];
        $methods = array('post', 'get', 'delete', 'put');
        $requestmethod = $_SERVER['REQUEST_METHOD'];

        //var_dump($request_uri);exit;

        $this->construct();

        $this->_apimethods = $this->getApiMethods();


    }

    public function handlerequest()
    {
        $request = $_REQUEST;
        return $this->_inithandle($request);
    }


    public function _inithandle($request)
    {

        //check if request_method is allowed
        $args = array();
        $methods = array('post', 'get', 'delete', 'put');
        $method = $_SERVER['REQUEST_METHOD'];
        $restoptions = array();


        if (in_array(strtolower($method), $methods)) {

            if(!array_key_exists('REDIRECT_PATH_INFO',$_SERVER)){
                $_SERVER['REDIRECT_PATH_INFO'] = "/";
            }
            if ($_SERVER['REDIRECT_PATH_INFO'] == "/") {
                $_SERVER['REDIRECT_PATH_INFO'] = "/allschedules";
            }
            $args['path'] = $_SERVER['REDIRECT_PATH_INFO'];
            $args['queryparams'] = $this->qeryParamsToArray($_SERVER['QUERY_STRING']);
            $args['method'] = $_SERVER['REQUEST_METHOD'];

            if (count($this->_apimethods) > 0) {


                foreach ($this->_apimethods as $k => $method) {


                    $reflection = new ReflectionAnnotatedMethod($method->getDeclaringClass()->getName(), $method->getName());
                    $originalstring = $reflection->getAnnotation('Path')->value;
                    $searchstring = preg_replace(array('/[{]([^}]*Id)[}]/si', '/[{]([^}]*Int)[}]/si',
                            '/[{]([^}]*Ids)[}]/si'), array('(\d+)', '(\d+)', '([0-4]|[0-4],[0-4]|[0-4],[0-4],[0-4]|[0-4],[0-4],[0-4],[0-4])'),
                        $originalstring);
                    $searchstring = str_replace("/", "\/", $searchstring) . "$";


                    if (preg_match("/" . $searchstring . "/i", $args['path'], $matches)) {


                        if (count($matches) > 1) {
                            unset($matches[0]);
                            sort($matches);
                        }


                        $tmp = explode("/", $originalstring);
                        $matches2 = array();
                        foreach ($tmp as $k => $v) {
                            if (preg_match("/[{]([^}]*)[}]/", $v)) {
                                array_push($matches2, $v);
                            }
                        }
                        $pathparams = array();
                        foreach ($matches2 as $k => $v) {

                            if (key_exists($k, $matches)) {
                                $key = str_replace("}", "", $v);
                                $key = str_replace("{", "", $key);
                                $pathparams[$key] = $matches[$k];
                            }

                        }

                        $args['pathparams'] = $pathparams;
                        $args['method_to_call'] = $method->getName();
                        if ($this->_db != null) {
                            $args['db'] = $this->_db;
                        }


                    }

                }

                if (array_key_exists('method_to_call', $args)) {

                    $this->_method = $args['method_to_call'];

                    try {
                        $object = $this->_apimethods[$args['method_to_call']]->getDeclaringClass()->newInstance();
                        $result = call_user_func_array(array($object, $args['method_to_call']),array($args));

                    } catch (Exception $e) {
                        $result = $this->fault($e, $e->getCode());
                    }


                } else {
                    $result = $this->fault(
                        new Novekinoxmlapi_Api_Exception("Dont have " . $args['method'] . " methods matched to request : " . $args['path']),
                        400
                    );
                }


            } else {

                $result = $this->fault(
                    new Novekinoxmlapi_Api_Exception("Dont have " . $args['method'] . " methods matched to request : " . $args['path']),
                    405
                );
            }

        } else {

            $result = $this->fault(
                new Novekinoxmlapi_Api_Exception("Request method: " . $method . " not allowed."),
                405
            );
        }

        $this->renderResponse($result, $restoptions);



    }

    public function getallheaders()
    {
        $headers = array();
        foreach ($_SERVER as $name => $value)
        {
            if (substr($name, 0, 5) == 'HTTP_')
            {
                $headers[str_replace(' ', '-', ucwords(strtolower(str_replace('_', ' ', substr($name, 5)))))] = $value;
            }
        }
        foreach ($this->_headers as $value)
        {
            $header = explode(':',$value);
            if(count($header) == 2){
                $headers[$header[0]] = trim($header[1]);
            }
        }


        if(isset($_SERVER['CONTENT_TYPE'])){
            $headers['Content-Type'] = $_SERVER['CONTENT_TYPE'];
            $this->_headers = array('Content-Type: '.$headers['Content-Type']);
        }

        if(!isset($headers['Content-Type']) || $headers['Content-Type'] == ""){
            $headers['Content-Type'] = "application/xml";
            $this->_headers = array('Content-Type: application/xml');
        }

        return $headers;
    }

    public function renderResponseJson($result,$restoptions = array())
    {
        return json_encode($result);
    }

    public function renderResponseXml($result,$restoptions = array())
    {
        if ($result instanceof SimpleXMLElement) {
            $response = $result->asXML();
        } elseif (is_string($result)) {
            $response = $result;
        } elseif ($result instanceof DOMDocument) {
            $response = $result->saveXML();
        } elseif ($result instanceof DOMNode) {
            $response = $result->ownerDocument->saveXML($result);
        } elseif (is_array($result) || is_object($result)) {
            $response = $this->_handleStruct($result, $restoptions);
        } else {
            $response = $this->_handleScalar($result, $restoptions);
        }

        return $response;
    }

    public function renderResponse($result,$restoptions = array())
    {
        $h = $this->getallheaders();

        switch ($h['Content-Type']){
            case "application/json":
                $response = $this->renderResponseJson($result,$restoptions);
                break;
            case "application/xml":
                $response = $this->renderResponseXml($result,$restoptions);
                break;
            default:
                $response = $this->fault(
                    new Novekinoxmlapi_Api_Exception("Request unknown content-type. Allowed application/json or application/xml"),
                    405
                );
                break;
        }




        if ($l = $this->getLogger()) $l->logInfo('Response:');
        if ($l = $this->getLogger()) $l->logInfo($response);
        if (!$this->returnResponse()) {
            if (!headers_sent()) {
                foreach ($this->_headers as $header) {
                    header($header);
                }
            }
            echo $response;
            return;
        }


        return $response;

    }


    /**
     * Implement Api::fault()
     *
     * Creates XML error response, returning DOMDocument with response.
     *
     * @param string|Exception $fault Message
     * @param int $code Error Code
     * @return string
     */
    public function fault($exception = null, $code = null)
    {

        if (isset($this->_apimethods[$this->_method])) {
            $method = $this->_apimethods[$this->_method];
        } elseif (isset($this->_method)) {
            $method = $this->_method;
        } else {
            $method = 'rest';
        }

        if ($method instanceof Zend_Server_Reflection_Function_Abstract) {
            $method = $method->getName();
        } else {
            $m = $method;
            $method = $m;
        }

        $msg = null;
        if ($exception instanceof Exception) {
            $msg = $exception->getMessage();
        } elseif (($exception !== null) || 'rest' == $method) {
            $msg = 'An unknown error occured. Please try again.';
        } else {
            $msg = 'An unknown error occured. Please try again.';
        }

        //$xmlMethod->appendChild($xmlResponse);
        //$xmlMethod->appendChild($dom->createElement('status', 'failed'));

        // Headers to send
        switch ($code) {
            case '403' :
                $this->_headers[] = 'HTTP/1.1 403 Forbidden';
                break;
            case '404' :
                $this->_headers[] = 'HTTP/1.1 404 Not Found';
                break;
            case '400':
            default:
                $this->_headers[] = 'HTTP/1.1 400 Bad Request';
        }

        if ($l = $this->getLogger()) $l->logError("Fault: $code, \n" . $msg);
        return $msg;
    }


    /**
     * Whether or not to return a response
     *
     * If called without arguments, returns the value of the flag. If called
     * with an argument, sets the flag.
     *
     * When 'return response' is true, {@link handle()} will not send output,
     * but will instead return the response from the dispatched function/method.
     *
     * @param boolean $flag
     * @return boolean.
     */
    public function returnResponse($flag = null)
    {
        if (null === $flag) {
            return $this->_returnResponse;
        }

        $this->_returnResponse = ($flag) ? true : false;
        return $this;
    }

    /**
     * Get XML encoding
     *
     * @return string
     */
    public function getEncoding()
    {
        return $this->_encoding;
    }

    /**
     * Handle a single value
     *
     * @param string|int|boolean $value Result value
     * @return string XML Response
     */
    protected function _handleScalar($value, $restoptions)
    {

        $method = array_key_exists($this->_method, $this->_apimethods) ? $this->_apimethods[$this->_method] : null;
        if ($method instanceof ReflectionMethod) {
            $class = $method->getDeclaringClass()->getName();
        } else {
            $class = false;
        }


        $method = method_exists($method, "getName") ? $method->getName() : $value;
        $dom = new DOMDocument('1.0', $this->getEncoding());
        if ($class) {
            $xml = $dom->createElement($class);
            $methodNode = $dom->createElement($method);
            $xml->appendChild($methodNode);
        } else {
            $xml = $dom->createElement($method);
            $methodNode = $xml;
        }
        $dom->appendChild($xml);

        if ($value === false) {
            $value = 0;
        } elseif ($value === true) {
            $value = 1;
        }

        if (isset($value)) {
            $element = $dom->createElement('response');
            $element->appendChild($dom->createTextNode($value));
            $methodNode->appendChild($element);
        } else {
            $methodNode->appendChild($dom->createElement('response'));
        }
        return $dom->saveXML();
    }

    /**
     * Handle an array or object result
     *
     * @param array|object $struct Result Value
     * @return string XML Response
     */
    protected function _handleStruct($struct, $restoptions = array())
    {

        if (isset($struct->enabledcdata) && $struct->enabledcdata == "true") {
            $restoptions['enablecdata'] = true;
        } else {
            $restoptions['enablecdata'] = false;
        }

        $parser = $this->getParser($restoptions);
        $xml = $parser->serialize($struct);
        return $xml;
    }


    public function qeryParamsToArray($q)
    {

        if (strlen($q) > 0) {

            $params = array();
            $tmp = explode("&", $q);
            foreach ($tmp as $k => $param) {
                $t = explode("=", $param);
                if (count($t) == 2) {
                    $params[$t[0]] = $t[1];
                } else {
                    $params[$t[0]] = false;
                }

            }
            return $params;

        } else {
            return array();
        }


    }

    public function getApiMethods()
    {

        //var_dump(get_class_methods());exit;
        $oReflectionClass = new ReflectionClass(get_class($this));
        $props = array();
        $propsanot = array();
        $matchedmethods = array();
        $requestmethod = $_SERVER['REQUEST_METHOD'];
        foreach ($oReflectionClass->getMethods() as $method) {

            $reflection = new ReflectionAnnotatedMethod($method->getDeclaringClass()->getName(), $method->getName());

            if ($reflection->getAnnotation($requestmethod)) {
                $matchedmethods[$method->getName()] = $method;
            }


        }

        return $matchedmethods;

    }

    public function getAllCategories()
    {
        $sql = "SELECT * FROM categories where category != ''";
        $categories = array();
        $db = $this->getDb();
        $sth = $db->prepare($sql);
        $sth->execute();
        $result = $sth->fetchAll(PDO::FETCH_ASSOC);
        $tmp = array();
        foreach ($result as $k => $cat) {
            $tmp[$cat['categoryid']] = $cat['category'];
        }

        return $tmp;

    }

    public function getScheduleObj($date, $data, $cats)
    {
        $s = new Schedule();
        $s->date = $date;
        $s->movies = array();
        foreach ($data as $j => $movies) {


            $m = new Movie();
            $m->movieid = $movies[0]['movie'];
            $m->title = trim(strip_tags($movies[0]['title']));

            $desc = str_replace(
                array('&#8222;','&#8221;','&#8211;','&#8217;'),
                array('"','"','-','\''),
                $movies[0]['description']
            );

            $desc = strip_tags($desc);
            $m->description = $desc;

            $m->coverurl = $movies[0]['coverurl'];
            $m->minutes = $movies[0]['minutes'];
            $m->shows = array();

            foreach ($movies as $i => $moviedesc) {
                array_push($m->shows, $moviedesc['showdate']);
            }

            if (intval($movies[0]['category']) > 0 && array_key_exists(intval($movies[0]['category']),$cats)) {
                $c = intval($movies[0]['category']);
                $m->category = $cats[$c];
            }
            array_push($s->movies, $m);
        }

        return $s;
    }


    /**
     * @GET
     * @Path("/test")
     **/
    public function getTest()
    {

    }

    /**
     * @GET
     * @Path("/allschedules")
     **/
    public function getAllSchedules()
    {

        $sql = "SELECT s.showdate,DATE(s.showdate) as day,s.movie,m.title,m.description,m.coverurl,m.minutes,
        m.category FROM schedules s, movies m where s.movie = m.movieid
        and DATE(s.showdate) >= DATE(now()) order by s.showdate;";
        $schedules = array();
        $db = $this->getDb();
        $sth = $db->prepare($sql);
        $sth->execute();
        $result = $sth->fetchAll(PDO::FETCH_ASSOC);
        if ($result == false) {
            return array();
        } else {
            $categories = $this->getAllCategories();

            $tmp = array();
            foreach ($result as $k => $movie) {
                $tmp[$movie['day']][$movie['movie']][] = $movie;
            }

            foreach ($tmp as $k => $schedule) {

                $scheduleObj = $this->getScheduleObj($k,$schedule,$categories);
                array_push($schedules, $scheduleObj);

            }
        }
        return $schedules;

    }

    /**
     * @GET
     * @Path("/day/{dayId}")
     **/
    public function getScheduleForDay($args)
    {

        if (array_key_exists('dayId', $args['pathparams']) && (intval($args['pathparams']['dayId']) <= 4 &&
                intval($args['pathparams']['dayId']) > -1)
        ) {

            $dayId = intval($args['pathparams']['dayId']);
            $date = date('Y-m-d', mktime(0, 0, 0, date("m"), date("d") + $dayId, date("Y")));
            $sql = "SELECT s.showdate,DATE(s.showdate) as day,s.movie,m.title,m.description,m.coverurl,m.minutes,
                        m.category FROM schedules s, movies m where s.movie = m.movieid
                        and DATE(s.showdate) = DATE(:date) order by s.showdate;";
            $schedules = array();
            $db = $this->getDb();
            $sth = $db->prepare($sql);
            $sth->execute(array(':date' => $date));
            $result = $sth->fetchAll(PDO::FETCH_ASSOC);

            if ($result == false) {
                $s = new Schedule();
                $s->date = $date;
                $s->movies = array();
                $result = $s;

            } else {

                if(count($result) > 0){

                    $categories = $this->getAllCategories();
                                    $tmp = array();
                                    foreach ($result as $k => $movie) {
                                        $tmp[$movie['day']][$movie['movie']][] = $movie;
                                    }

                                    foreach ($tmp as $k => $schedule) {


                                        $scheduleObj = $this->getScheduleObj($k,$schedule,$categories);
                                        array_push($schedules, $scheduleObj);

                                    }


                }else{

                }

            }

            if ($schedules > 0) {
                $result = $schedules;
            } else {
                $result = $this->fault(
                    new Novekinoxmlapi_Api_Exception("Schedules not found for this criteria: " . $args['path']),
                    400
                );
            }


        } else {

            $result = $this->fault(
                new Novekinoxmlapi_Api_Exception("Schedules not found for this criteria: " . $args['path']),
                400
            );

        }
        if (count($result) == 0) {
            $tmp = array();
            $s = new Schedule();
                            $s->date = $date;
                            $s->movies = array();


        array_push($tmp,$s);
            $result = $tmp;
        }

        return $result;

    }

    /**
     * @GET
     * @Path("/fromdays/{daysIds}")
     **/
    public function getScheduleFromDays($args)
    {

        $result = false;
        if (array_key_exists('daysIds', $args['pathparams'])) {

            $dayIds = explode(",", $args['pathparams']['daysIds']);
            $dayIds = array_unique($dayIds);
            $tmp = array();
            foreach ($dayIds as $k => $v) {
                $date = date('Y-m-d', mktime(0, 0, 0, date("m"), date("d") + $v, date("Y")));
                array_push($tmp, $date);
            }

            if (count($tmp) > 0) {

                $sql = "";
                $schedules = array();
                $db = $this->getDb();
                $sth = $db->prepare($sql);

                $qMarks = str_repeat('?,', count($tmp) - 1) . '?';
                $sth = $db->prepare("SELECT s.showdate,DATE(s.showdate) as day,s.movie,m.title,m.description,m.coverurl,m.minutes,
                                                                        m.category FROM schedules s, movies m where s.movie = m.movieid
                                                                        and DATE(s.showdate) IN ($qMarks) order by s.showdate;");
                $sth->execute($tmp);

                $result = $sth->fetchAll(PDO::FETCH_ASSOC);
                if ($result == false) {
                    $s = new Schedule();
                    $s->date = $date;
                    $s->movies = array();
                    $result = $s;
                } else {
                    $categories = $this->getAllCategories();
                    $tmp = array();
                    foreach ($result as $k => $movie) {
                        $tmp[$movie['day']][$movie['movie']][] = $movie;
                    }

                    foreach ($tmp as $k => $schedule) {

                        $scheduleObj = $this->getScheduleObj($k, $schedule, $categories);
                        array_push($schedules, $scheduleObj);

                    }
                }

                if ($schedules > 0) {
                    $result = $schedules;
                } else {
                    $result = $this->fault(
                        new Novekinoxmlapi_Api_Exception("Schedules not found for this criteria: " . $args['path']),
                        400
                    );
                }

            }


        } else {

            $result = $this->fault(
                new Novekinoxmlapi_Api_Exception("Schedules not found for this criteria: " . $args['path']),
                400
            );

        }

        return $result;


    }


}

