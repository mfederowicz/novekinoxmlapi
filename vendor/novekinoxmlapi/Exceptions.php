<?php
/**
 * @see Novekinoxmlapi_Abstract
 */
class Novekinoxmlapi_Exception extends Exception {

    public function __construct($message = null, $code = 0, Exception $previous = null)
    {
        parent::__construct($message, $code, $previous);

        error_log($message);
    }


}
class Novekinoxmlapi_Parser_Exception extends Novekinoxmlapi_Exception {}
class Novekinoxmlapi_Api_Exception extends Novekinoxmlapi_Exception {}

