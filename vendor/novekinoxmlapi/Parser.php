<?php
/**
 * @see Novekinoxmlapi_Abstract
 */

require_once 'Abstract.php';
use Guzzle\Http\Client;

class Novekinoxmlapi_Parser extends Novekinoxmlapi_Abstract
{

    /**
     * Class constructor
     */
    public function __construct()
    {
        $this->construct();

    }

    /**
     * Parse cinema schedule from now to next 4 days
     */
    public function doStuff($all = false)
    {
        //header('Content-Type: text/html; charset=utf-8');
        if ($all == true) {

            for ($i = 0; $i < $this->_maxdays; $i++) {
                $date = date('Y-m-d', mktime(0, 0, 0, date("m"), date("d") + $i, date("Y")));
                $schedule = $this->parsedate($this->_repertuar_url, $date);
                print $schedule . "<br />";

            }

        } else {

            $date = date('Y-m-d', mktime(0, 0, 0, date("m"), date("d"), date("Y")));
            $schedule = $this->parsedate($this->_repertuar_url, $date);
            print $schedule . "<br />";


        }

        $this->removeDuplicates();

    }


    public function parsedate($uri, $date)
    {
        $this->clearSchedulesFromDate($date);
        $client = $this->getClient();
        try {
            $uri = $uri . $date;
            $elementsInfo = array();
            $request = $client->get($uri, array('timeout' => 20));
            $response = $request->send();
            $result = $response->getMessage();
            if ($result != "") {

                phpQuery::newDocument($result);
                $tmp = pq('table[class="repertoire-list"]');

                //var_dump($tmp->find('tr'));exit;
                foreach ($tmp->find('tr') as $k => $tr) {

                    if($k == 0 ){
                        continue;
                    }

                    phpQuery::newDocument(pq($tr)->html());
                    $tmp = pq('div[class="repertoire-movie-poster"] a');
                    $href = $tmp->attr('href');
                    $movieId = intval(str_replace("film.php?id=", "", $href));

                    if ($movieId > 0) {


                        $tmp = pq('div[class="repertoire-movie-tag"]');
                        $genre = $this->enc($tmp->html());


                        $tmp = pq('div[class="repertoire-movie-times"]');
                        $tmphours = strip_tags($tmp->html());
                        preg_match_all('/\d{2}\:\d{2}/', $tmphours, $matches);
                        if (count($matches) > 0) {
                            $matches = array_unique($matches[0]);
                        }
                        $hours = $matches;

                        //check movie in db
                        $movieObj = $this->checkMovieInDb($movieId);

                        if (!$movieObj) {
                            //header('Content-type: text/xml');
                            //var_dump(mb_convert_encoding($title, "UTF-8","ISO-8859-2"));exit;

                            $genreId = $this->getGenreId($genre);
                            //getting movie stuff
                            $moviedata = $this->getMovieData($movieId);
                            /*if($movieId == 3034){
                                var_dump($moviedata);exit;
                            }*/
                            //getting movie picture url
                            $url_image = $moviedata['picture'];

                            $time = null;
                            if (array_key_exists('minutes', $moviedata['movieparams'])) {
                                $time = $moviedata['movieparams']['minutes'];
                            }

                            $this->addMovieToDb($movieId, $moviedata['title'], $moviedata['description'],$genreId,
                                $url_image, $time);


                        }


                        //We can add existing movie to schedule
                        if (count($hours) > 0) {

                            foreach ($hours as $j => $h) {

                                $showdate = $date . " " . $h . ":00";

                                $this->addMovieToSchedule($movieId, $showdate);

                            }

                        }

                    }


                }
                $elementsInfo = "Movies from:" . $date . " added to DB";

            }
            return $elementsInfo;
        } catch (Exception $e) {
            //var_dump($e->getMessage());exit;
            return "Movies from:" . $date . " not added to DB try again later";
        }
    }

    public function enc($string)
    {
        return mb_convert_encoding($string, "UTF-8", "ISO-8859-2");
    }

    public function denc($string)
    {
        return mb_convert_encoding($string, "ISO-8859-2", "UTF-8");
    }

    public function addMovieToSchedule($movieId, $showdate)
    {

        $db = $this->getDb();
        $st = $db->prepare('INSERT INTO schedules(showdate,movie) VALUES(:showdate,:movie)');
        $st->execute(array(':showdate' => $showdate, ':movie' => $movieId));
        return true;

    }

    public function clearSchedulesFromDate($date)
        {

            $db = $this->getDb();
            $st = $db->prepare('select id from schedules where date(showdate) = date(:d)');
            $st->execute(array(':d' => $date));
            $ar = $st->fetchAll(PDO::FETCH_ASSOC);

            // start transaction
            $db->beginTransaction();

            foreach ($ar as $k => $row) {

                $id = $row['id'];
                $st = $db->prepare('DELETE FROM schedules WHERE id = :id');
                $st->execute(array(':id' => $id));
            }

            // end transaction
            $db->commit();
            return true;

        }


    public function removeDuplicates()
    {

        $db = $this->getDb();
        $st = $db->prepare('select id from schedules where id not in (select min(id) from schedules as x group by showdate,movie   having  count(*) >= 1)');
        $st->execute();
        $ar = $st->fetchAll(PDO::FETCH_ASSOC);

        // start transaction
        $db->beginTransaction();

        foreach ($ar as $k => $row) {

            $id = $row['id'];
            $st = $db->prepare('DELETE FROM schedules WHERE id = :id');
            $st->execute(array(':id' => $id));
        }

        // end transaction
        $db->commit();
        return true;

    }

    public function addMovieToDb($movieId, $title, $description, $genreId, $url_image, $time)
    {

        $db = $this->getDb();
        try {
            $st = $db->prepare('INSERT INTO movies(title,description,coverurl,movieid,minutes,
                            category) VALUES(:title,:description,:coverurl,:movieid,:minutes,:category)');
            $st->execute(
                array(
                    ':title' => $title,
                    ':description' => $description,
                    ':coverurl' => $url_image,
                    ':movieid' => $movieId,
                    ':minutes' => $time,
                    ':category' => $genreId
                )
            );
            $result = $st->fetch(PDO::FETCH_OBJ);


        } catch (Exception $e) {
            print $e->getMessage();
            exit;
            return null;
        }


        return $result;

    }

    public function getGenreId($genre)
    {

        $genreId = null;
        $db = $this->getDb();
        $sth = $db->prepare('SELECT categoryid FROM categories WHERE category = :category');
        $sth->execute(array(':category' => $genre));
        $result = $sth->fetch(PDO::FETCH_OBJ);
        if ($result == false) {
            $st = $db->prepare('INSERT INTO categories(category) VALUES(:name)');
            $st->execute(array(':name' => $genre));
            $result = $st->fetch(PDO::FETCH_OBJ);
            $genreId = $result->categoryid;
        } else {
            $genreId = $result->categoryid;
        }


        return intval($genreId);
    }

    public function getSchedule()
    {


        $db = $this->getDb();
        $sth = $db->prepare('SELECT * FROM schedules s, movies m where s.movie = m.movieid');
        $sth->execute();
        $result = $sth->fetchAll(PDO::FETCH_ASSOC);
        //header('Content-type: text/xml');
        header('Content-Type: text/html; charset=utf-8');
        foreach ($result as $k => $row) {
            $t = mb_detect_encoding($row['title']);

            echo $t . ":" . $row['title'] . "<br />";
        }


    }


    public function checkMovieInDb($movieId)
    {
        $movieStatus = false;
        if ($movieId > 0) {

            $db = $this->getDb();
            $sth = $db->prepare('SELECT * FROM movies WHERE movieid = :movieid');
            $sth->execute(array(':movieid' => $movieId));
            $result = $sth->fetch(PDO::FETCH_OBJ);
            $movieStatus = $result;

        }
        return $movieStatus;
    }

    public function getMoviePicture($movieId)
    {
        $moviePicture = null;
        try {

            $client = $this->getClient();
            $uri = $this->_movie_url . $movieId;
            $request = $client->get($uri, array('timeout' => 20));
            $response = $request->send();
            $result = $response->getBody();
            phpQuery::newDocument($result);
            $tmp = pq('img[class="photo"]');
            $moviePicture = $tmp->attr('src');
            return $moviePicture;


        } catch (Exception $e) {
            $e->getMessage();
            return null;
        }


    }

    public function getMovieData($movieId)
    {
        $moviedata = array();
        try {

            $client = $this->getClient();
            $uri = $this->_movie_url . $movieId;

            $request = $client->get($uri, array('timeout' => 20));
            $response = $request->send();
            $result = $response->getBody();
            phpQuery::newDocument($result);
            //get title
            $tmp = pq('div[class="movie_header-title_info"]');
            $title = trim(strip_tags($tmp->find('h2')->html()));
            //var_dump($this->enc($title));exit;
            //get description
            $tmp = pq('section[class="text_panel panel"]');
            $movieDescription = null;
            foreach ($tmp->find('p') as $k => $p) {
                if (pq($p)->html() != "") {
                    $movieDescription .= pq($p)->html() . "\r\n";
                }
            }
            //movie params
            $tmp = pq('div[class="movie_header-info"]');
            $params = array();
            foreach ($tmp->find('span') as $k => $p) {
                if (pq($p)->html() != "") {
                    if($k == 0){
                        $params['genre'] = pq($p)->html();
                    }
                    if($k == 1){
                        $minutes = str_replace('minut','', intval(pq($p)->html()));
                        $params['minutes'] = $minutes;
                    }
                }
            }

            //photo url
            $tmp = pq('div[class="movie_details_panel-poster"] img');
            $moviePicture = $tmp->attr('src');

            $moviedata = array('title' => $title, 'description' => $movieDescription,
                'movieparams' => $params,
                'picture' => $moviePicture);
        } catch (Exception $e) {
            $e->getMessage();
            //print $e->getMessage();exit;
            $moviedata = null;
        }

        return $moviedata;


    }


    public function updateMoviesDescriptions()
    {

        $db = $this->getDb();
        $st = $db->prepare('select movieid from movies');
        $st->execute();
        $ar = $st->fetchAll(PDO::FETCH_ASSOC);


        foreach ($ar as $k => $row) {

            $id = $row['movieid'];
            $moviedata = $this->getMovieData($id);
            $this->updateMovieDescription($id,$moviedata['description']);

        }


        return true;

    }

    public function updateMovieDescription($id, $description)
    {

        $db = $this->getDb();
        $st = $db->prepare('UPDATE movies set description = :desc where movieid = :id');
        $st->execute(array(':desc' => $description, ':id' => $id));
        return true;

    }



}

