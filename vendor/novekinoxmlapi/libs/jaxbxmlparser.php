<?php
/**
 * Class Jaxbxmlparser
 * With this class you can unserialize for (jaxb)xml -> (php)obj
 * and serialize for (php)obj -> (jaxb)xml
 * with support for extended Annotations
 * @author created by: Marcin Federowicz
 * @author last changed by: $Author$
 * @version $Rev$ $Date$
 */

require_once 'annotations_parser.php';

class GET extends Annotation
{
}
class POST extends Annotation
{
}
class PUT extends Annotation
{
}
class DELETE extends Annotation
{
}
class Path extends Annotation
{
}
class Proptype extends Annotation
{
}

class Contains extends Annotation
{
}

class ApiMethod extends Annotation
{
}

class ApiPath extends Annotation
{
}


class Jaxbxmlparser
{

    /*

    NodeTypes
    1 	ELEMENT_NODE
    2 	ATTRIBUTE_NODE
    3 	TEXT_NODE
    4 	CDATA_SECTION_NODE
    5 	ENTITY_REFERENCE_NODE
    6 	ENTITY_NODE
    7 	PROCESSING_INSTRUCTION_NODE
    8 	COMMENT_NODE
    9 	DOCUMENT_NODE
    10 	DOCUMENT_TYPE_NODE
    11 	DOCUMENT_FRAGMENT_NODE
    12 	NOTATION_NODE

    */

    /**
     * Options for the unserialization
     *
     * @access public
     * @var    array
     */
    var $options = array();
    var $defaultOptions = array('objtagprefix' => 'pl.novekino.xml.api.',
        'collectiontag' => 'collection',
        'collectiontags' => array('collection','list','vector','set'),
        'objects2strings' => array()
    );

    /**
     * Array of references from xml
     *
     * @access public
     * @var    array
     */
    var $references = array();

    /**
     * Array of objects to build references
     *
     * @access public
     * @var    array
     */
    var $objectslist = array();

    /**
     * tags counter
     *
     * @access public
     * @var    integer
     */
    var $tagscounter = 1;

    /**
     * objects counter
     *
     * @access public
     * @var    integer
     */
    var $objectscounter = 1;

    /**
     * Constructor
     *
     * @param mixed $options array containing options for unserialization
     *
     * @access public
     */
    function Jaxbxmlparser($options = null)
    {

        if (is_array($options)) {
            $this->options = array_merge($this->defaultOptions, $options);
        } else {
            $this->options = $this->defaultOptions;
        }
    }

    /**
     * unserialize data
     *
     * @param xml $xml    xml to unserialize
     * @param array $options options array
     * @return obj
     */
    function unserialize($xml = null, $options = null)
    {
        if (isset($xml)) {
            $dom = new DomDocument();
            $dom->preserveWhiteSpace = false;

            //Hack invalid signs in xml. Proper should be in CDATA
            $xml = preg_replace('/[\x00-\x08\x10\x0B\x0C\x0E-\x19\x7F]|\x1A|\x1E|\x1F'.
                '|(?<=^|[\x00-\x7F])[\x80-\xBF]+'.
                '|([\xC0\xC1]|[\xF0-\xFF])[\x80-\xBF]*'.
                '|[\xC2-\xDF]((?![\x80-\xBF])|[\x80-\xBF]{2,})' .
                '|[\xE0-\xEF](([\x80-\xBF](?![\x80-\xBF]))|(?![\x80-\xBF]{2})|[\x80-\xBF]{3,})/',
                '�', $xml );
            $xml = preg_replace('/\xE0[\x80-\x9F][\x80-\xBF]'.
                '|\xED[\xA0-\xBF][\x80-\xBF]/S','?', $xml );

            libxml_use_internal_errors(true);
            if ((@simplexml_load_string($xml) instanceof SimpleXMLElement) == false) {
                $messageTemplate = "Error: %s at line: %d, col; %d";
                $message = '';
                foreach (libxml_get_errors() as $error) {
                    $message .= sprintf($messageTemplate, $error->message, $error->line, $error->column);
                }
                libxml_clear_errors();

                throw new Exception ('invalid xml from interface: ' . $message);
            } else {
                libxml_use_internal_errors(true);
                $dom->loadXML($xml);
                $root = $dom->documentElement;
                $rootid = $root->hasAttribute('id') ? $root->getAttribute('id') : $this->objectscounter++;
                $this->references = array();
                $rootname = $root->localName;

                if (in_array($rootname,$this->options['collectiontags']) && count($root->childNodes) > 0) {

                    if ($root->firstChild != null) {

                        if ($root->firstChild->localName == "string" || in_array($root->firstChild->localName,$this->options['objects2strings'])) {
                            $ob = $this->process_listxmlstring($root->childNodes);
                        } elseif ($root->firstChild->localName == "int") {
                            $ob = $this->process_listxmlint($root->childNodes);
                        } else {

                            $ob = $this->process_listxmlob($root->childNodes);

                        }

                    } else {

                        $ob = null;
                    }

                } elseif ($rootname == "int") {

                    $ob = new Int;
                    $ob->integer = $root->nodeValue;


                } elseif ($rootname == "string") {

                    $ob = new String;
                    $ob->string = $root->nodeValue;


                } else {
                    $obid = $root->hasAttribute('id') ? $root->getAttribute('id') : $this->objectscounter++;
                    $tmp = explode(".", $root->localName);
                    $objname = end($tmp);
                    $obj = new $objname;
                    $this->references[$obid] = $obj;
                    $ob = $this->process_xmlob($obj, $root->childNodes);

                }
                $only_objects = array();
                $only_objects_ids = array();
                foreach ($this->references as $k => $v) {

                    if (is_object($v)) {
                        $objectid = spl_object_hash($v);
                        $only_objects[$k] = $v;
                        $only_objects_ids[$k] = $objectid;
                    }

                }



                return $ob;

            }


        } else {
            return null;
        }
    }

    /**
     * process list of xml strings
     *
     * @param xml $xml    xml to unserialize
     * @return array of strings or one string
     *
     */
    function process_listxmlstring($data)
    {
        //global $this->references;
        $array = array();
        foreach ($data as $string) {
            $array[] = $string->nodeValue;
        }
        $result = null;
        if (count($array) > 1) {
            $result = $array;
        } elseif (count($array) == 1) {
            $result = $array[0];
        }
        return $result;

    }


    /**
     * process list of xml objects
     *
     * @param xml $xml    xml to unserialize
     * @return array of objects or one object
     *
     */
    function process_listxmlob($data)
    {
        //global $this->references;
        $objects = array();
        foreach ($data as $object) {
            $obid = $object->hasAttribute('id') ? $object->getAttribute('id') : $this->objectscounter++;
            $tmp = explode(".", $object->localName);
            $objname = end($tmp);

            $obj = new $objname;
            $this->references[$obid] = $obj;
            $objects[] = $this->process_xmlob($obj, $object->childNodes);
        }
        $result = null;
        if (count($objects) > 1) {
            $result = $objects;
        } elseif (count($objects) == 1) {
            $result = $objects[0];
        }

        return $result;

    }

    /**
     * process xml object
     *
     * @param obj $ob    object created before process this method
     * @param xml $data    xml to unserialize
     * @return unserialized object
     *
     */
    function process_xmlob($ob, $data)
    {

        //check data counter
        $initprops = array();
        foreach ($data as $i => $v) {
            $initprops[$v->localName] = $v->nodeValue;
        }

        $oReflectionClass = new ReflectionAnnotatedClass($ob);

        if (count($initprops) > count($oReflectionClass->getProperties())) {

            //regen object
            foreach ($initprops as $i => $p) {
                $ob->$i = $p;
            }

        }


        $props = array();
        $propsanot = array();

        foreach ($oReflectionClass->getProperties() as $prop) {

            $propname = $prop->name;
            $props[$propname] = $prop->name;

            if($prop->hasAnnotation('Proptype')){
            $propsanot[$propname] = array('prop' => $prop->name,
                                          'type' => ($prop->hasAnnotation('Proptype') != null)
                                                  ? $prop->getAnnotation('Proptype')->value : "string",
                                          'contains' => ($prop->hasAnnotation('Contains') != null)
                                                  ? $prop->getAnnotation('Contains')->value : null);

            }else{
            $propsanot[$propname] = array('prop' => $prop->name,
                                          'type' => ($this->getTypefromAnotation($prop->getDocComment()) != null)
                                                  ? $this->getTypefromAnotation($prop->getDocComment()) : "string");
            }


        }

        //var_dump($propsanot);exit;

        /**
         * walk through the data and check annotations for each element
         */
        foreach ($data as $p) {

            if (in_array($p->localName, $props)) {

                $key = $p->localName;
                $value = $p->nodeValue;

                if ($propsanot[$key]['type'] == "boolean") {

                    if ($p->hasAttribute('reference')) {
                        $refid = $p->getAttribute('reference');
                        $ob->$key = $this->references[$refid];
                    } elseif ($p->hasAttribute('id')) {
                        $id = $p->getAttribute('id');
                        $this->references[$id] = $p->nodeValue;
                        $ob->$key = $p->nodeValue;
                    } elseif (!$p->hasAttribute('id') && !$p->hasAttribute('reference')) {
                        $ob->$key = $p->nodeValue;

                    }

                } elseif ($propsanot[$key]['type'] == "int" || $propsanot[$key]['type'] == "integer"
                          || $propsanot[$key]['type'] == "integerdouble"
                          || $propsanot[$key]['type'] == "integerlong") {


                    if ($p->hasAttribute('reference')) {
                        $refid = $p->getAttribute('reference');
                        $ob->$key = $this->references[$refid];
                    } elseif ($p->hasAttribute('id')) {
                        $id = $p->getAttribute('id');
                        $this->references[$id] = $p->nodeValue;
                        $ob->$key = $p->nodeValue;
                    } elseif (!$p->hasAttribute('id') && !$p->hasAttribute('reference')) {
                        $ob->$key = $p->nodeValue;
                    }

                } elseif ($propsanot[$key]['type'] == "long") {

                    if ($p->hasAttribute('reference')) {
                        $refid = $p->getAttribute('reference');
                        $ob->$key = $this->references[$refid];
                    } elseif ($p->hasAttribute('id')) {
                        $id = $p->getAttribute('id');
                        $this->references[$id] = $p->nodeValue;
                        $ob->$key = $p->nodeValue;
                    } elseif (!$p->hasAttribute('id') && !$p->hasAttribute('reference')) {
                        $ob->$key = $p->nodeValue;
                    }

                } elseif ($propsanot[$key]['type'] == "string") {

                    if ($p->hasAttribute('reference')) {
                        $refid = $p->getAttribute('reference');
                        $ob->$key = $this->references[$refid];

                    } elseif ($p->hasAttribute('id')) {
                        $id = $p->getAttribute('id');
                        $this->references[$id] = $p->nodeValue;
                        $ob->$key = $p->nodeValue;
                    } elseif (!$p->hasAttribute('id') && !$p->hasAttribute('reference')) {
                        $ob->$key = $p->nodeValue;
                    }

                } elseif ($propsanot[$key]['type'] == "stringdomain") {

                    $tmp = explode(".", $p->localName);
                    $elementname = end($tmp);
                    if ($p->hasAttribute('reference')) {
                        $refid = $p->getAttribute('reference');

                        $ob->$elementname = $this->references[$refid];

                    } elseif ($p->hasAttribute('id')) {
                        $id = $p->getAttribute('id');
                        $this->references[$id] = $p->nodeValue;
                        $ob->$elementname = $p->nodeValue;
                    } elseif (!$p->hasAttribute('id') && !$p->hasAttribute('reference')) {
                        $ob->$elementname = $p->nodeValue;
                    }

                } elseif ($propsanot[$key]['type'] == "stringclass") {

                    if ($p->hasAttribute('reference')) {
                        $refid = $p->getAttribute('reference');
                        $ob->$key = $this->references[$refid];
                    } elseif ($p->hasAttribute('id')) {
                        $id = $p->getAttribute('id');
                        $this->references[$id] = $p->nodeValue;
                        $ob->$key = $p->nodeValue;
                    } elseif (!$p->hasAttribute('id') && !$p->hasAttribute('reference')) {
                        $ob->$key = $p->nodeValue;
                    }

                } elseif ($propsanot[$key]['type'] == "stringid") {

                    if ($p->hasAttribute('reference')) {
                        $refid = $p->getAttribute('reference');
                        $ob->$key = $this->references[$refid];
                    } elseif ($p->hasAttribute('id')) {
                        $id = $p->getAttribute('id');
                        $this->references[$id] = $p->nodeValue;
                        $ob->$key = $p->nodeValue;
                    } elseif (!$p->hasAttribute('id') && !$p->hasAttribute('reference')) {
                        $ob->$key = $p->nodeValue;
                    }

                } elseif ($propsanot[$key]['type'] == "dateTime") {

                    if ($p->hasAttribute('reference')) {
                        $refid = $p->getAttribute('reference');
                        $ob->$key = $this->references[$refid];
                    } else {
                        $id = $p->hasAttribute('id') ? $p->getAttribute('id') : $this->objectscounter++;
                        ;
                        $this->references[$id] = $p->nodeValue;
                        $ob->$key = $p->nodeValue;
                    }

                } elseif ($propsanot[$key]['type'] == "LargeAccountType") {

                    if ($p->hasAttribute('reference')) {

                        $refid = $p->getAttribute('reference');

                        $ob->$key = $this->references[$refid];
                    } elseif ($p->hasAttribute('id')) {
                        $id = $p->getAttribute('id');
                        $this->references[$id] = $p->nodeValue;
                        $ob->$key = $p->nodeValue;
                    }

                } elseif ($propsanot[$key]['type'] == "Object" &&
                          !preg_match("/MapdomainstringOf|MapstringOf|MapOf|ArrayOf/", $propsanot[$key]['type'])) {


                    if ($p->hasAttribute('reference')) {
                        $refid = $p->getAttribute('reference');
                        $ob->$key = $this->references[$refid];
                    } else {
                        $obid = $p->hasAttribute('id') ? $p->getAttribute('id') : $this->objectscounter++;
                        $objname = $propsanot[$key]['contains'];

                        $obj = new $propsanot[$key]['contains'];
                        $this->references[$obid] = $obj;
                        $ob->$key = $this->process_xmlob($obj, $p->childNodes);
                    }
                } elseif ($propsanot[$key]['type'] == "MapOf") {

                    if ($p->hasAttribute('reference')) {
                        $refid = $p->getAttribute('reference');
                        $ob->$key = $this->references[$refid];
                    } elseif ($p->hasAttribute('id')) {

                        $array = array();
                        foreach ($p->childNodes as $node) {

                            $array[] = array("entry" => $this->process_xmlmapentry($node));

                        }
                        if (count($array) > 0) {
                            $ob->$key = $array;
                        } else {
                            $ob->$key = array();
                        }

                    }
                } elseif ($propsanot[$key]['type'] == "MapdomainstringOf") {


                    if ($p->hasAttribute('reference')) {

                        $refid = $p->getAttribute('reference');

                        $ob->$key = $this->references[$refid];
                    } elseif ($p->hasAttribute('id')) {
                        $array = array();
                        foreach ($p->childNodes as $node) {

                            $array[] = array("entry" => $this->process_xmlmapentrydomainstring($node));

                        }
                        if (count($array) > 0) {
                            $ob->$key = $array;
                        } else {
                            $ob->$key = array();
                        }

                    }
                } elseif ($propsanot[$key]['type'] == "MapstringOf" ||
                          (preg_match("/MapstringOf/", $propsanot[$key]['type']) &&
                           $propsanot[$key]['contains'] == null)) {


                    if ($p->hasAttribute('reference')) {

                        $refid = $p->getAttribute('reference');

                        $ob->$key = $this->references[$refid];
                    } elseif ($p->hasAttribute('id')) {
                        $array = array();
                        foreach ($p->childNodes as $node) {

                            $array[] = array("entry" => $this->process_xmlmapentrystring($node));

                        }
                        if (count($array) > 0) {
                            $ob->$key = $array;
                        } else {
                            $ob->$key = array();
                        }

                    }
                } elseif (($propsanot[$key]['type'] == "ArrayOf" && $propsanot[$key]['contains'] != null &&
                           !preg_match("/Int|String|DomainString$/", $propsanot[$key]['type'])) ||
                          (preg_match("/^ArrayOf/", $propsanot[$key]['type']) &&
                           !key_exists('contains',$propsanot[$key]) &&
                           !preg_match("/Int|String|DomainString$/", $propsanot[$key]['type']))
                          ) {

                    if ($p->hasAttribute('reference')) {
                        $refid = $p->getAttribute('reference');
                        $ob->$key = $this->references[$refid];
                    } else {

                        $array = array();
                        foreach ($p->childNodes as $node) {

                            $objname = get_class($node);
                            if($objname == "DOMElement"){
                            if ($node->hasAttribute('reference')) {
                                $refid = $node->getAttribute('reference');
                                $array[] = $this->references[$refid];
                            } else {
                                $obid = $node->hasAttribute('id')
                                        ? $node->getAttribute('id') : $this->objectscounter++;
                                $tmp = explode(".", $node->localName);
                                $objname = end($tmp);

                                if (preg_match("/DTO$/", $objname) || $propsanot[$key]['type'] != null) {

                                    $obj = new $objname;

                                    $this->references[$obid] = $obj;
                                    $array[] = $this->process_xmlob($obj, $node->childNodes);
                                }

                            }
                            }

                        }

                        if (count($array) > 0) {
                            $ob->$key = $array;
                        } else {
                            $ob->$key = array();
                        }

                        //var_dump($ob->$key);


                    }
                } elseif (preg_match("/^ArrayOfInt$/", $propsanot[$key]['type'])) {
                    if ($p->hasAttribute('reference')) {
                        $refid = $p->getAttribute('reference');
                        $ob->$key = $this->references[$refid];
                    } elseif ($p->hasAttribute('id')) {
                        $array = array();
                        foreach ($p->childNodes as $node) {
                            $array[]['int'] = $node->nodeValue;
                        }
                        if (count($array) > 0) {
                            $ob->$key = $array;
                        } else {
                            $ob->$key = array();
                        }

                    }
                } elseif (preg_match("/^ArrayOfString$/", $propsanot[$key]['type'])) {
                    if ($p->hasAttribute('reference')) {
                        $refid = $p->getAttribute('reference');
                        $ob->$key = $this->references[$refid];
                    } else {
                        $array = array();
                        foreach ($p->childNodes as $node) {
                            $array[]['string'] = $node->nodeValue;
                        }
                        if (count($array) > 0) {
                            $ob->$key = $array;
                        } else {
                            $ob->$key = array();
                        }

                    }
                } elseif (preg_match("/^ArrayOfDomainString$/", $propsanot[$key]['type'])) {
                    if ($p->hasAttribute('reference')) {
                        $refid = $p->getAttribute('reference');
                        $ob->$key = $this->references[$refid];
                    } elseif ($p->hasAttribute('id')) {
                        $array = array();
                        foreach ($p->childNodes as $node) {

                            $tmp = explode(".", $node->localName);
                            $keyname = end($tmp);
                            $array[][$keyname] = $node->nodeValue;
                        }
                        if (count($array) > 0) {
                            $ob->$key = $array;
                        } else {
                            $ob->$key = array();
                        }

                    }
                }elseif ($propsanot[$key]['type'] == "MapstringsOf" ||
                    (preg_match("/MapstringsOf/", $propsanot[$key]['type']) &&
                        $propsanot[$key]['contains'] == null)) {


                    if ($p->hasAttribute('reference')) {

                        $refid = $p->getAttribute('reference');

                        $ob->$key = $this->references[$refid];
                    } elseif ($p->hasAttribute('id')) {
                        $array = array();
                        foreach ($p->childNodes as $node) {

                            $array[] = array("entry" => $this->process_xmlmapentrystrings($node));

                        }
                        if (count($array) > 0) {
                            $ob->$key = $array;
                        } else {
                            $ob->$key = array();
                        }

                    }
                }

            }

        }

        foreach ($props as $p) {
            if ($ob->$p == "NULL") {
                //unset($ob->$p);
            }
        }

        //var_dump($ob);exit;

        return $ob;

    }


    /**
     * process xml map entry domain string
     *
     * @param xml $data    xml to unserialize
     * @return unserialized object
     *
     */

    function process_xmlmapentrydomainstring($data)
    {
        //global $references;
        $o = new MapEntryDomainString();
        foreach ($data->childNodes as $node) {


            $tagname = $node->localName;

            if (!preg_match("/DTO$/", $tagname)) {

                $tmp = explode(".", $node->localName);
                $elementname = end($tmp);

                $o->$elementname = $node->nodeValue;
                $stringid = $node->getAttribute('id');
                $this->references[$stringid] = $node->nodeValue;

            } else {
                $obid = $node->getAttribute('id');
                $tmp = explode(".", $node->localName);
                $objname = end($tmp);
                $obx = new $objname;
                $this->references[$obid] = $obx;
                $o->value = $this->process_xmlob($obx, $node->childNodes);

            }


        }

        return $o;


    }


    /**
     * process xml map entry string
     *
     * @param xml $data    xml to unserialize
     * @return unserialized object
     *
     */

    function process_xmlmapentrystring($data)
    {
        //global $references;
        $o = new MapEntryString();
        foreach ($data->childNodes as $node) {
            $tagname = $node->localName;
            if ($tagname == "string") {
                $o->$tagname = $node->nodeValue;
            } else {

                if ($node->hasAttribute('reference')) {
                    $refid = $node->getAttribute('reference');
                    $o->value = $this->references[$refid];
                } else {
                    $obid = $node->getAttribute('id');
                    $tmp = explode(".", $node->localName);
                    $objname = end($tmp);
                    $obx = new $objname;
                    $this->references[$obid] = $obx;
                    $o->value = $this->process_xmlob($obx, $node->childNodes);
                }


            }


        }

        return $o;


    }

    /**
     * process xml map entry strings
     *
     * @param xml $data    xml to unserialize
     * @return unserialized object
     *
     */

    function process_xmlmapentrystrings($data)
    {
        //var_dump($data);exit;
        //global $references;
        $o = array();
        foreach ($data->childNodes as $node) {
            $tagname = $node->localName;
            if ($tagname == "string") {
                array_push($o,$node->nodeValue);
            }


        }

        return $o;


    }

    /**
     * process xml map entry
     *
     * @param xml $data    xml to unserialize
     * @return unserialized object
     *
     */
    function process_xmlmapentry($data)
    {


        $o = new MapEntry();
        foreach ($data->childNodes as $node) {


            $tagname = $node->localName;

            if ($tagname == "int") {
                $o->$tagname = $node->nodeValue;
            } else {

                $obid = $node->getAttribute('id');
                $tmp = explode(".", $node->localName);
                $objname = end($tmp);
                $obx = new $objname;
                $this->references[$obid] = $obx;
                $o->value = $this->process_xmlob($obx, $node->childNodes);

            }


        }

        return $o;


    }

    /**
     * get type from annotation string
     *
     * @param string $string    string to parse
     * @return properyty type string
     *
     */
    function getTypefromAnotation($string)
    {
        $string = str_replace("*", "", $string);
        $string = str_replace("/", "", $string);
        return trim($string);
    }

    /**
     * magic autoload for classes
     *
     * @param classname
     *
     * @access public
     */
    function __autoload($name)
    {
        echo "Want to load $name.\n";
        throw new MissingException("Unable to load $name.");
    }

    /**
     * dump dom element as xml
     *
     * @param DomElement
     * @return xml;
     *
     */

    function _asxml(DOMElement $element)
    {
        return simplexml_import_dom($element)->asXML();
    }

    /**
     * new dom document with one tag
     *
     * @param tagname
     * @return object DOMDocument;
     *
     */

    function _newdom($tagname)
    {
        $newdoc = new DOMDocument('1.0', 'UTF-8');
        $newdoc->formatOutput = true;
        $newdoc->preserveWhiteSpace = true;

        // Add some markup
        $tag = $newdoc->createElement($tagname);
        $newdoc->appendChild($tag);


        return $newdoc;
    }

    /**
     * get InvalidParameterException object based on xml
     *
     * @param mixed $data    data to unserialize
     * @return InvalidParameterException object or false;
     *
     */
    function getInvalidParameterException($data, $xpath)
    {
        try {
            $objname = str_replace("get", "", __FUNCTION__);
            $obj = new $objname();
            foreach ($data->childNodes as $node) {
                $tagname = $node->localName;
                switch ($tagname) {
                    case "errorDetails":
                        $traces = array();
                        foreach ($node->childNodes as $el) {

                            foreach ($el->childNodes as $i => $ch) {


                                if ($ch->localName == "string") {
                                    $traces['entry'][] = $ch->nodeValue;
                                } elseif ($ch->localName == "int") {
                                    $traces['entry'][] = $ch->nodeValue;
                                } elseif ($ch->localName == $this->options['collectiontag']) {
                                    //$traces['entry'][] = "LIST";
                                }


                            }

                        }


                        //print_r($traces);exit;
                        $tab = array_values($traces);

                        $formated = array();
                        if (count($tab) > 0) {

                            foreach ($tab as $k => $v) {


                                foreach ($v as $kk => $vv) {

                                    //print $k.($kk%2).":".$vv."<br />";

                                    if (($kk % 2)) {
                                        $formated[] = $vv;

                                    }


                                }

                                //$formated['%'.$k.'%'] = $v[1];

                            }

                        }

                        $tmpformated = array();

                        foreach ($formated as $k => $v) {

                            $tmpformated['%' . $k . '%'] = $v;
                        }


                        $obj->$tagname = $tmpformated;
                        break;
                    case "stackTrace":
                        $traces = array();
                        foreach ($node->childNodes as $el) {
                            $traces[] = $el->nodeValue;
                        }
                        $obj->$tagname = $traces;
                        break;
                    default:
                        $obj->$tagname = $node->nodeValue;
                        break;
                }
            }
            return $obj;
        } catch (Exception $e) {
            echo $e->getMessage(), "\n";
            return false;
        }
    }

    function unserializeInvalidParameterException($data, $xpath)
    {

        $tmp = explode(".", $data->item(0)->localName);
        $objname = end($tmp);
        $methodname = "get" . $objname;
        if (method_exists($this, $methodname)) {

            return $this->$methodname($data->item(0), $xpath);

        } else {
            return false;
        }

    }

    /**
     * get FailedException object based on xml
     *
     * @param mixed $data    data to unserialize
     * @return InvalidParameterException object or false;
     *
     */
    function getFailedException($data, $xpath)
    {
        try {


            //var_dump();
            $s = simplexml_import_dom($data);

            return $s;

            return $obj;
        } catch (Exception $e) {
            echo $e->getMessage(), "\n";
            return false;
        }
    }


    function unserializeFailedException($data, $xpath)
    {

        $tmp = explode(".", $data->item(0)->localName);
        $objname = end($tmp);
        $methodname = "get" . $objname;
        if (method_exists($this, $methodname)) {

            return $this->$methodname($data->item(0), $xpath);

        } else {
            return false;
        }

    }

    /**
     * unserialize exception
     *
     * @param xml $xml    xml to unserialize
     * @param array $options options array
     *
     */
    function unserializeexception($xml)
    {
        $dom = new DOMDocument('1.0', 'UTF-8');
        $dom->preserveWhiteSpace = false;
        $dom->loadXml($xml);
        $tmp = explode(".", $dom->firstChild->localName);
        $classname = end($tmp);
        $methodname = "unserialize" . $classname;

        if (method_exists($this, $methodname)) {
            $xpath = new DOMXpath($dom);
            $data = $xpath->query("/*");
            return $this->$methodname($data, $xpath);
        } else {
            return false;
        }
    }


    /**
     * serialize object or array of objects to xml
     *
     * @param array $obj of objects or only one object to serialize
     * @return xml in xstream format;
     *
     */
    function serialize($obj)
    {
        try {

            $newdoc = new DOMDocument('1.0', 'UTF-8');
            if (is_array($obj)) {

                //print "array";
                $rootname = $this->options['collectiontag'];
                $root = $newdoc->createElement($rootname);
                $rootnode = $newdoc->appendChild($root);

                foreach ($obj as $object) {

                    if(is_object($object)){

                        $objectid = spl_object_hash($object);

                        //$this->tagscounter++;
                        $counter = $this->tagscounter++;
                        $this->objectslist[$counter] = $objectid;
                        $tags = $this->serialize_object($object);
                        $objname = get_class($object);
                        $rootname = $this->options['objtagprefix'] . $objname;
                        $newDom = $this->_newdom($rootname);
                        foreach ($tags as $domElement) {
                            $domNode = $newDom->importNode($domElement, true);
                            $newDom->documentElement->appendChild($domNode);
                        }
                        $node = $newDom->getElementsByTagName($rootname)->item(0);
                        $node = $newdoc->importNode($node, true);
                        $newdoc->documentElement->appendChild($node);

                    }elseif(is_string($object)){

                        $child = $newdoc->createElement('string',$this->cleanstring($object));
                        $newdoc->documentElement->appendChild($child);


                    }


                }

            } elseif (is_object($obj)) {

                $objectid = spl_object_hash($obj);

                //$this->tagscounter++;
                $counter = $this->tagscounter++;
                $this->objectslist[$counter] = $objectid;

                $tags = $this->serialize_object($obj);

                $objname = get_class($obj);
                if(preg_match("/DTO/", $objname)){
                $rootname = $this->options['objtagprefix'] . $objname;
                }else{
                $rootname = $objname;
                }


                $newDom = $this->_newdom($rootname);

                foreach ($tags as $domElement) {
                    $domNode = $newDom->importNode($domElement, true);
                    $newDom->documentElement->appendChild($domNode);
                }

                $newdoc = $newDom;


            }

            return $newdoc->saveXML();
        } catch (Exception $e) {
            echo $e->getMessage(), "\n";
            return false;
        }

    }

    /**
     * serialize php object to xml
     *
     * @param $obj object  to serialize
     * @return xml in xstream format;
     *
     */
    function serialize_object($obj)
    {

        $initprops = array();

        foreach ($obj as $i => $v) {
            $initprops[$i] = $v;
        }

        $oReflectionClass = new ReflectionAnnotatedClass($obj);

        $props = array();
        $propsanot = array();
        if (count($initprops) > count($oReflectionClass->getProperties())) {

            //regen object
            foreach ($initprops as $i => $p) {
                $obj->$i = $p;
                $propname = $i;
                $props[$propname] = $i;
                $propsanot[$propname] = array('prop' => $i,'type' => "string");
            }
        }else{
        foreach ($oReflectionClass->getProperties() as $prop) {
            $propname = $prop->name;
            $props[$propname] = $prop->name;
            if($prop->hasAnnotation('Proptype')){
            $propsanot[$propname] = array('prop' => $prop->name,
                                          'type' => ($prop->hasAnnotation('Proptype') != null)
                                                  ? $prop->getAnnotation('Proptype')->value : "string",
                                          'contains' => ($prop->hasAnnotation('Contains') != null)
                                                  ? $prop->getAnnotation('Contains')->value : null);

            }else{
            $propsanot[$propname] = array('prop' => $prop->name,
                                          'type' => ($this->getTypefromAnotation($prop->getDocComment()) != null)
                                                  ? $this->getTypefromAnotation($prop->getDocComment()) : "string");
            }
        }
        }

        $objectid = spl_object_hash($obj);



        //$this->tagscounter++;

        /**
         * walk through the data and check annotations for each element
         */
        $enablecdata = key_exists('enablecdata',$this->options) && $this->options['enablecdata'] == true?true:false;
        $newdom = $this->_newdom("tmp");
        foreach ($obj as $key => $p) {


            if (in_array($key, $props)) {

                if ($propsanot[$key]['type'] == "boolean") {

                    //print $key.":".strlen($p);
                    if (strlen($p)) {
                        $child = $newdom->createElement($key, $p);
                        $newdom->documentElement->appendChild($child);
                    }


                } elseif ($propsanot[$key]['type'] == "integerlong") {

                    if ($p != NULL) {
                        $child = $newdom->createElement($key, $p);
                        $newdom->documentElement->appendChild($child);
                    }

                } elseif ($propsanot[$key]['type'] == "integer") {

                    if ($p != NULL) {
                        $child = $newdom->createElement($key, $p);
                        $newdom->documentElement->appendChild($child);
                    }


                } elseif ($propsanot[$key]['type'] == "integerdouble") {

                    if ($p != NULL) {
                        $child = $newdom->createElement($key, $p);
                        $newdom->documentElement->appendChild($child);
                    }


                } elseif ($propsanot[$key]['type'] == "long") {

                    //$counter = $this->tagscounter++;


                    if ($p != NULL) {

                        $child = $newdom->createElement($key, $p);
                        $newdom->documentElement->appendChild($child);
                    }

                } elseif ($propsanot[$key]['type'] == "string") {

                    if (is_object($p)) {
                        $oReflectionClass = new ReflectionObject($p);
                        if($oReflectionClass->getName() == "OCI-Lob"){
                        $child = $newdom->createElement($key, $this->cleanstring($p->load()));
                        $newdom->documentElement->appendChild($child);
                        }
                    } else {


                        if (!is_array($p) && strlen($p) > 0) {
                            //$counter = $this->tagscounter++;
                            if($enablecdata == true){

                            $child = $newdom->createElement($key);
                            $child->appendChild($newdom->createCDATASection($this->cleanstring($p)));
                            }else{
                            $child = $newdom->createElement($key,$this->cleanstring($p));
                            }


                            $newdom->documentElement->appendChild($child);
                        }
                    }

                } elseif ($propsanot[$key]['type'] == "stringclass") {

                    //$child = $newdom->createElement($key,$p);
                    if ($p != NULL) {
                        $child = $newdom->createElement($key, $this->cleanstring($p));

                        $newdom->documentElement->appendChild($child);
                    }


                } elseif ($propsanot[$key]['type'] == "stringid") {

                    if ($p != NULL) {
                        $md5 = md5($p);


                        $counter = $this->tagscounter++;
                        $this->objectslist[$counter] = $md5;
                        $child = $newdom->createElement($key, $this->cleanstring($p));
                        $newdom->documentElement->appendChild($child);

                    }


                } elseif ($propsanot[$key]['type'] == "dateTime") {


                    if ($p != NULL) {


                        $child = $newdom->createElement($key, $p);
                        $counter = $this->tagscounter++;
                        $x = $newdom->documentElement->appendChild($child);

                        $pos = strpos($p, "CE");
                        if ($pos === false) {
                            //$x->setAttribute("class", "sql-timestamp");
                        }

                        //$x->setAttribute("id", $counter);


                    }


                } elseif (($propsanot[$key]['type'] == "Object" &&
                          !preg_match("/MapstringOf|MapdomainstringOf|MapOf|ArrayOf/", $propsanot[$key]['type']))
                          || (preg_match("/DTO/", $propsanot[$key]['type']) &&
                          !preg_match("/MapstringOf|MapdomainstringOf|MapOf|ArrayOf/", $propsanot[$key]['type']))) {


                    if (is_object($p)) {

                        $objectid = spl_object_hash($p);
                        if (in_array($objectid, $this->objectslist)) {
                            $child = $newdom->createElement($key);
                            $refid = array_search($objectid, $this->objectslist);
                            $newdom->documentElement->appendChild($child)->setAttribute("reference", $refid);

                        } else {

                            $counter = $this->tagscounter++;
                            $this->objectslist[$counter] = $objectid;

                            $tags = $this->serialize_object($p);
                            $objname = get_class($p);
                            $rootname = $key;

                            $newDom = $this->_newdom($rootname);
                            //$newDom->documentElement->setAttribute("id", $counter);

                            foreach ($tags as $domElement) {
                                $domNode = $newDom->importNode($domElement, true);
                                $newDom->documentElement->appendChild($domNode);
                            }


                            //var_dump($newDom->documentElement);
                            $node = $newDom->getElementsByTagName($rootname)->item(0);
                            $node = $newdom->importNode($node, true);
                            $newdom->documentElement->appendChild($node);


                        }

                    }


                } elseif (($propsanot[$key]['type'] == "MapOf" && $propsanot[$key]['contains'] != null) ||
                          (preg_match("/MapOf/", $propsanot[$key]['type']) &&
                           !key_exists('contains',$propsanot[$key]))) {

                    $md5 = md5($propsanot[$key]['type'] . $key . $objectid);
                    if (in_array($md5, $this->objectslist)) {
                        //print "in";
                        $child = $newdom->createElement($key);
                        $refid = array_search($md5, $this->objectslist);
                        $newdom->documentElement->appendChild($child)->setAttribute("reference", $refid);
                    } else {

                        $counter = $this->tagscounter++;
                        $this->objectslist[$counter] = $md5;


                        if ($p != NULL) {

                            $tmpdom = $this->_newdom("tmp");

                            foreach ($p as $k => $v) {

                                $tags = $this->serialize_mapentry($v);
                                $entryDom = $this->_newdom("entry");
                                foreach ($tags as $domElement) {
                                    $domNode = $entryDom->importNode($domElement, true);
                                    $entryDom->documentElement->appendChild($domNode);
                                }

                                //var_dump($newDom->documentElement);
                                $node = $entryDom->getElementsByTagName("entry")->item(0);
                                $node = $tmpdom->importNode($node, true);
                                $tmpdom->documentElement->appendChild($node);


                            }

                            $tags = $tmpdom->firstChild->childNodes;

                            $newDom = $this->_newdom($key);

                            foreach ($tags as $domElement) {
                                $domNode = $newDom->importNode($domElement, true);
                                $newDom->documentElement->appendChild($domNode);
                            }

                            $node = $newDom->getElementsByTagName($key)->item(0);
                            $node = $newdom->importNode($node, true);
                            $newdom->documentElement->appendChild($node)->setAttribute("id", $counter);
                        }


                    }


                } elseif ($propsanot[$key]['type'] == "MapstringOf" ||
                          (preg_match("/MapstringOf/", $propsanot[$key]['type']) &&
                           !key_exists('contains',$propsanot[$key]))) {


                    $md5 = md5($propsanot[$key]['type'] . $key . $objectid);
                    if (in_array($md5, $this->objectslist)) {
                        //print "in";
                        $child = $newdom->createElement($key);
                        $refid = array_search($md5, $this->objectslist);
                        $newdom->documentElement->appendChild($child)->setAttribute("reference", $refid);
                    } else {
                        //print "add";
                        $counter = $this->tagscounter++;
                        $this->objectslist[$counter] = $md5;

                        if ($p != NULL) {

                            $tmpdom = $this->_newdom("tmp");

                            foreach ($p as $k => $v) {
                                $tags = $this->serialize_mapentrystring($v);
                                $entryDom = $this->_newdom("entry");
                                foreach ($tags as $domElement) {
                                    $domNode = $entryDom->importNode($domElement, true);
                                    $entryDom->documentElement->appendChild($domNode);
                                }

                                //var_dump($newDom->documentElement);
                                $node = $entryDom->getElementsByTagName("entry")->item(0);
                                $node = $tmpdom->importNode($node, true);
                                $tmpdom->documentElement->appendChild($node);


                            }

                            $tags = $tmpdom->firstChild->childNodes;

                            $newDom = $this->_newdom($key);

                            foreach ($tags as $domElement) {
                                $domNode = $newDom->importNode($domElement, true);
                                $newDom->documentElement->appendChild($domNode);
                            }

                            $node = $newDom->getElementsByTagName($key)->item(0);
                            $node = $newdom->importNode($node, true);
                            $newdom->documentElement->appendChild($node)->setAttribute("id", $counter);

                        }
                    }

                } elseif ($propsanot[$key]['type'] == "MapdomainstringOf" ||
                          (preg_match("/MapdomainstringOf/", $propsanot[$key]['type']) &&
                           !key_exists('contains',$propsanot[$key]))) {


                    $md5 = md5($propsanot[$key]['type'] . $key . $objectid);
                    if (in_array($md5, $this->objectslist)) {
                        //print "in";
                        $child = $newdom->createElement($key);
                        $refid = array_search($md5, $this->objectslist);
                        $newdom->documentElement->appendChild($child)->setAttribute("reference", $refid);
                    } else {
                        //print "add";
                        $counter = $this->tagscounter++;
                        $this->objectslist[$counter] = $md5;

                        if ($p != NULL) {

                            $tmpdom = $this->_newdom("tmp");

                            foreach ($p as $k => $v) {

                                $tags = $this->serialize_mapentrydomainstring($v);
                                $entryDom = $this->_newdom("entry");
                                foreach ($tags as $domElement) {
                                    $domNode = $entryDom->importNode($domElement, true);
                                    $entryDom->documentElement->appendChild($domNode);
                                }

                                //var_dump($newDom->documentElement);
                                $node = $entryDom->getElementsByTagName("entry")->item(0);
                                $node = $tmpdom->importNode($node, true);
                                $tmpdom->documentElement->appendChild($node);


                            }

                            $tags = $tmpdom->firstChild->childNodes;

                            $newDom = $this->_newdom($key);

                            foreach ($tags as $domElement) {
                                $domNode = $newDom->importNode($domElement, true);
                                $newDom->documentElement->appendChild($domNode);
                            }

                            $node = $newDom->getElementsByTagName($key)->item(0);
                            $node = $newdom->importNode($node, true);
                            $newdom->documentElement->appendChild($node)->setAttribute("id", $counter);

                        }
                    }

                } elseif (($propsanot[$key]['type'] == "ArrayOf" && $propsanot[$key]['contains'] != null &&
                           !preg_match("/Int|String$/", $propsanot[$key]['type'])
                          ) || (preg_match("/^ArrayOf/", $propsanot[$key]['type'])  &&
                           !preg_match("/Int|String$/", $propsanot[$key]['type']) &&
                                !key_exists('contains',$propsanot[$key])
                          )
                ) {


                    $md5 = md5($propsanot[$key]['type'] . $key . $objectid . count($p));
                    if (in_array($md5, $this->objectslist)) {
                        //print "in";
                        $child = $newdom->createElement($key);
                        $refid = array_search($md5, $this->objectslist);
                        $newdom->documentElement->appendChild($child)->setAttribute("reference", $refid);
                    } else {
                        //print "add";
                        $counter = $this->tagscounter++;



                        if (count($p) > 0 && is_array($p)) {
                            $this->objectslist[$counter] = $md5;
                            $tmpcounter = $counter;
                            $tmpdom = $this->_newdom("tmp");
                            foreach ($p as $object) {

                                $objectid = spl_object_hash($object);

                                if (in_array($objectid, $this->objectslist)) {
                                    //print "ref";
                                    $objname = get_class($object);
                                    if (preg_match("/DTO/", $objname)) {
                                        $rootname = $this->options['objtagprefix'] . $objname;
                                    } else {
                                        $rootname = $objname;
                                    }

                                    $childref = $tmpdom->createElement($rootname);
                                    $refid = array_search($objectid, $this->objectslist);
                                    $tmpdom->documentElement->appendChild($childref)
                                            ->setAttribute("reference", $refid);

                                } else {

                                    $counter = $this->tagscounter++;
                                    $this->objectslist[$counter] = $objectid;
                                    $tags = $this->serialize_object($object);
                                    $objname = get_class($object);
                                    if (preg_match("/DTO/", $objname)) {
                                        $rootname = $this->options['objtagprefix'] . $objname;
                                    } else {
                                        $rootname = $objname;
                                    }
                                    $newDom = $this->_newdom($rootname);

                                    foreach ($tags as $domElement) {
                                        $domNode = $newDom->importNode($domElement, true);
                                        $newDom->documentElement->appendChild($domNode);
                                    }
                                    $node = $newDom->getElementsByTagName($rootname)->item(0);
                                    $node = $tmpdom->importNode($node, true);
                                    $tmpdom->documentElement->appendChild($node);


                                }


                                //$newdoc->documentElement->appendChild($node);
                            }

                            $tags = $tmpdom->firstChild->childNodes;
                            $newDom = $this->_newdom($key);

                            foreach ($tags as $domElement) {
                                $domNode = $newDom->importNode($domElement, true);
                                $newDom->documentElement->appendChild($domNode);
                            }

                            $node = $newDom->getElementsByTagName($key)->item(0);
                            $node = $newdom->importNode($node, true);
                            $newdom->documentElement->appendChild($node);


                        } else {
                            $child = $newdom->createElement($key);
                            $newdom->documentElement->appendChild($child);

                        }
                    }

                } elseif (preg_match("/^ArrayOfInt$/", $propsanot[$key]['type'])) {

                    $md5 = md5($propsanot[$key]['type'] . $key . $objectid);
                    if (in_array($md5, $this->objectslist)) {
                        //print "in";
                        $child = $newdom->createElement($key);
                        $refid = array_search($md5, $this->objectslist);
                        $newdom->documentElement->appendChild($child)->setAttribute("reference", $refid);
                    } else {
                        //print "add";
                        $counter = $this->tagscounter++;
                        $this->objectslist[$counter] = $md5;
                        if (count($p) > 0 && is_array($p)) {


                            $tmpcounter = $counter;
                            $tmpdom = $this->_newdom("tmp");
                            foreach ($p as $k => $v) {

                                $child = $tmpdom->createElement("int", $v['int']);
                                $tmpdom->documentElement->appendChild($child);

                            }
                            $tags = $tmpdom->firstChild->childNodes;
                            $newDom = $this->_newdom($key);

                            foreach ($tags as $domElement) {
                                $domNode = $newDom->importNode($domElement, true);
                                $newDom->documentElement->appendChild($domNode);
                            }

                            $node = $newDom->getElementsByTagName($key)->item(0);
                            $node = $newdom->importNode($node, true);
                            $newdom->documentElement->appendChild($node)->setAttribute("id", $tmpcounter);

                        } else {

                            $child = $newdom->createElement($key);
                            $newdom->documentElement->appendChild($child)->setAttribute("id", $counter);

                        }

                    }

                } elseif (preg_match("/^ArrayOfString$/", $propsanot[$key]['type'])) {

                    $md5 = md5($propsanot[$key]['type'] . $objectid);

                    //print "add";
                    $counter = $this->tagscounter++;
                    $this->objectslist[$counter] = $md5;
                    if (count($p) > 0 && is_array($p)) {


                        $tmpcounter = $counter;
                        $tmpdom = $this->_newdom("tmp");
                        foreach ($p as $k => $v) {

                            if (is_array($v) && key_exists('string', $v)) {

                                $string = $v['string'];
                            }

                            if (!is_array($v) && $v != "") {

                                $string = $v;
                            }


                            $child = $tmpdom->createElement("string", $string);
                            $tmpdom->documentElement->appendChild($child);

                        }

                        /*header('Content-type: text/xml');
                         print $tmpdom->saveXML();exit;*/


                        $tags = $tmpdom->firstChild->childNodes;
                        $newDom = $this->_newdom($key);

                        foreach ($tags as $domElement) {
                            $domNode = $newDom->importNode($domElement, true);
                            $newDom->documentElement->appendChild($domNode);
                        }

                        $node = $newDom->getElementsByTagName($key)->item(0);
                        $node = $newdom->importNode($node, true);
                        $newdom->documentElement->appendChild($node);

                    } else {

                        $child = $newdom->createElement($key);
                        //$newdom->documentElement->appendChild($child)->setAttribute("id", $counter);

                    }


                    /*header('Content-type: text/xml');
                   print $newdom->saveXML();exit;*/

                } elseif ($propsanot[$key]['type'] == "MapstringsOf" ||
                    (preg_match("/MapstringsOf/", $propsanot[$key]['type']) &&
                        !key_exists('contains',$propsanot[$key]))) {


                    $md5 = md5($propsanot[$key]['type'] . $key . $objectid);
                    if (in_array($md5, $this->objectslist)) {
                        //print "in";
                        $child = $newdom->createElement($key);
                        $refid = array_search($md5, $this->objectslist);
                        $newdom->documentElement->appendChild($child)->setAttribute("reference", $refid);
                    } else {
                        //print "add";
                        $counter = $this->tagscounter++;
                        $this->objectslist[$counter] = $md5;

                        if ($p != NULL) {

                            $tmpdom = $this->_newdom("tmp");
                            foreach ($p as $k => $v) {
                                $tags = $this->serialize_mapentrystrings($v['entry']);
                                $entryDom = $this->_newdom("entry");
                                foreach ($tags as $domElement) {
                                    $domNode = $entryDom->importNode($domElement, true);
                                    $entryDom->documentElement->appendChild($domNode);
                                }

                                //var_dump($newDom->documentElement);
                                $node = $entryDom->getElementsByTagName("entry")->item(0);
                                $node = $tmpdom->importNode($node, true);
                                $tmpdom->documentElement->appendChild($node);


                            }

                            $tags = $tmpdom->firstChild->childNodes;

                            $newDom = $this->_newdom($key);

                            foreach ($tags as $domElement) {
                                $domNode = $newDom->importNode($domElement, true);
                                $newDom->documentElement->appendChild($domNode);
                            }

                            $node = $newDom->getElementsByTagName($key)->item(0);
                            $node = $newdom->importNode($node, true);
                            $newdom->documentElement->appendChild($node)->setAttribute("id", $counter);

                        }
                    }



                }

            }
        }


        /*header('Content-type: text/xml');
       print $newdom->saveXML();exit;*/

        return $newdom->firstChild->childNodes;

    }

    function cleanstring($string)
    {

        $enablecdata = key_exists('enablecdata',$this->options) && $this->options['enablecdata'] == true?true:false;

        $search = array('Ĺź', '&', 'nullstring','');
        $replace = array('Â°', '&amp;', '','');
        $string = str_replace($search, $replace, $string);
        $string = stripslashes($string);

        if($enablecdata == true){

        }

        return $string;

    }

    /**
     * serialize php object to xml
     *
     * @param object to serialize
     * @return xml in xstream format;
     *
     */
    function serialize_mapentry($obj)
    {
        $dom = $this->_newdom("tmp");


        foreach ($obj as $k => $object) {

            $int = $dom->createElement("int", $object->int);
            $dom->documentElement->appendChild($int);

            $objectid = spl_object_hash($object->value);


            if (in_array($objectid, $this->objectslist)) {
                //print "in";

                $refid = array_search($objectid, $this->objectslist);

                $objname = get_class($object->value);
                $rootname = $this->options['objtagprefix'] . $objname;

                $obj = $dom->createElement($rootname);
                $dom->documentElement->appendChild($obj)->setAttribute("reference", $refid);


            } else {
                //print "add";
                $counter = $this->tagscounter++;
                $this->objectslist[$counter] = $objectid;

                $tags = $this->serialize_object($object->value);
                $objname = get_class($object->value);
                $rootname = $this->options['objtagprefix'] . $objname;
                $newDom = $this->_newdom($rootname);
                $newDom->documentElement->setAttribute("id", $counter);

                foreach ($tags as $domElement) {
                    $domNode = $newDom->importNode($domElement, true);
                    $newDom->documentElement->appendChild($domNode);
                }

                $node = $newDom->getElementsByTagName($rootname)->item(0);
                $node = $dom->importNode($node, true);
                $dom->documentElement->appendChild($node);


            }

        }

        return $dom->firstChild->childNodes;

    }

    /**
     * serialize mapentrystring php object to xml
     *
     * @param object to serialize
     * @return xml in xstream format;
     *
     */
    function serialize_mapentrystring($obj)
    {
        $dom = $this->_newdom("tmp");


        foreach ($obj as $k => $object) {

            $int = $dom->createElement("string", $object->string);
            $dom->documentElement->appendChild($int);

            $objectid = spl_object_hash($object->value);


            if (in_array($objectid, $this->objectslist)) {
                //print "in";

                $refid = array_search($objectid, $this->objectslist);

                $objname = get_class($object->value);
                $rootname = $this->options['objtagprefix'] . $objname;

                $obj = $dom->createElement($rootname);
                $dom->documentElement->appendChild($obj)->setAttribute("reference", $refid);

            } else {
                //print "add";
                $counter = $this->tagscounter++;
                $this->objectslist[$counter] = $objectid;

                $tags = $this->serialize_object($object->value);
                $objname = get_class($object->value);
                $rootname = $this->options['objtagprefix'] . $objname;
                $newDom = $this->_newdom($rootname);
                $newDom->documentElement->setAttribute("id", $counter);

                foreach ($tags as $domElement) {
                    $domNode = $newDom->importNode($domElement, true);
                    $newDom->documentElement->appendChild($domNode);
                }

                $node = $newDom->getElementsByTagName($rootname)->item(0);
                $node = $dom->importNode($node, true);
                $dom->documentElement->appendChild($node);

            }

        }

        return $dom->firstChild->childNodes;

    }

    /**
     * serialize mapentrystrings php object to xml
     *
     * @param object to serialize
     * @return xml in xstream format;
     *
     */
    function serialize_mapentrystrings($obj)
    {
        $dom = $this->_newdom("tmp");
        foreach ($obj as $k => $object) {

            $int = $dom->createElement("string", $object);
            $dom->documentElement->appendChild($int);

        }

        return $dom->firstChild->childNodes;

    }

    /**
     * serialize mapentrydomainstring php object to xml
     *
     * @param object to serialize
     * @return xml in xstream format;
     *
     */
    function serialize_mapentrydomainstring($obj)
    {
        $dom = $this->_newdom("tmp");


        foreach ($obj as $k => $object) {

            $oReflectionClass = new ReflectionObject($object);
            $props = array();
            $propsanot = array();
            foreach ($oReflectionClass->getProperties() as $prop) {
                $propname = $prop->name;
                if ($prop->name != "value") {
                    $tagname = $prop->name;
                }
            }

            if ($tagname == "string") {
                $xmltagname = "string";
            } else {
                $xmltagname = $this->options['objtagprefix'] . $tagname;

            }


            $int = $dom->createElement($xmltagname, $object->$tagname);
            $counter = $this->tagscounter++;
            $dom->documentElement->appendChild($int)->setAttribute("id", $counter);

            $objectid = spl_object_hash($object->value);


            if (in_array($objectid, $this->objectslist)) {
                //print "in";

                $refid = array_search($objectid, $this->objectslist);

                $objname = get_class($object->value);
                $rootname = $this->options['objtagprefix'] . $objname;

                $obj = $dom->createElement($rootname);
                $dom->documentElement->appendChild($obj)->setAttribute("reference", $refid);

            } else {
                //print "add";
                $counter = $this->tagscounter++;
                $this->objectslist[$counter] = $objectid;

                $tags = $this->serialize_object($object->value);
                $objname = get_class($object->value);
                $rootname = $this->options['objtagprefix'] . $objname;
                $newDom = $this->_newdom($rootname);
                $newDom->documentElement->setAttribute("id", $counter);

                foreach ($tags as $domElement) {
                    $domNode = $newDom->importNode($domElement, true);
                    $newDom->documentElement->appendChild($domNode);
                }

                $node = $newDom->getElementsByTagName($rootname)->item(0);
                $node = $dom->importNode($node, true);
                $dom->documentElement->appendChild($node);

            }

        }

        return $dom->firstChild->childNodes;

    }

    /**
     * get objects list element
     *
     * @param object to serialize
     * @return xml in xstream format;
     *
     */
    function getobjectref($hash)
    {

        $refid = array_search($hash, $_SESSION['objects_references_ids']);
        return $_SESSION['objects_references'][$refid];

    }

    /**
     * reset objectslist
     */
    function resetobjectlist()
    {

        $this->objectslist = array();
        $this->tagscounter = 1;
    }


}


?>