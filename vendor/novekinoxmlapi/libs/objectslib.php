<?php
/*
 *  @(#)novekinoxmlapi.objectslib.php
 */

class Base {
    /**
     * @Proptype("integer")
     **/
    public $id;
}

class Schedule extends Base
{
    /**
     * @Proptype("string")
     **/
    public $date;

    /**
     * @Proptype("ArrayOf")
     * @Contains("Movie")
     **/
    public $movies;

}

class Movie extends Base
{
    /**
     * @Proptype("integer")
     **/
    public $id;
    /**
     * @Proptype("integer")
     **/
    public $movieid;
    /**
     * @Proptype("string")
     **/
    public $title;
    /**
     * @Proptype("string")
     **/
    public $origtitle;
    /**
     * @Proptype("string")
     **/
    public $description;
    /**
     * @Proptype("string")
     **/
    public $coverurl;
    /**
     * @Proptype("integer")
     **/
    public $minutes;
    /**
     * @Proptype("string")
     **/
    public $category;
    /**
     * @Proptype("ArrayOfStringType")
     **/
    public $shows;
}

class StringType {
    /**
     * @Proptype("string")
     **/
    public $string;

}

class Message {
    /**
     * @Proptype("string")
     **/
    public $string;

}